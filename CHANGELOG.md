## [1.0.23.0] - 2020-01-14

### Changed

* User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to access configuration (JSON) file. 
* User friendly message with detailed error description has been added when user is unable to start/stop CCCAgent BIS Service through CCCAgent CTI Connector Administration.
* User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to get running status for CCCAgent BIS Service.
* User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to update system settings.
* * User friendly message with detailed error description has been added when CCCAgent CTI Connector Administration is unable to connect to NEC 3C API.

### Added
* Proxy address field has been added to support proxy setup for outgoing traffic.
* Settings for internal extension length and outbound dialing prefix has been added to match contact details for external incoming calls.
* Friendly name field has been added in PABX configuration for better readability.

### Fixed
* Connection to PCG is lost on clicking transfer button from Rainbow Web UI.
* CCCAgent has to be restarted manually after a PCG upgrade.
* Invalid Phone Number" error on external outbound calls.
* CCCAgent has to be restarted manually after a disconnection with NEC 3C API.